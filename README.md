# Générateur de réapprovisionnement - eCig & Zen

**Télécharger pour utilisation**

Une boite à outils de convertisseur csv.

**Si vous avez un problème contacté [moi](https://www.leffeweb.com/).**

## To Use
Télécharger le fichier & lancer le.

```bash
# Clone this repository
git clone https://leffeweb@bitbucket.org/leffeweb/generateur.git
# Go into the repository
cd generateur
# Install dependencies
npm install
# Run the app
npm start
```

Note: J'utilisais Linux, pour tout problème de système d'exploitation voir les docs.

## Resources for Learning Electron

- [electronjs.org/docs](https://electronjs.org/docs) - all of Electron's documentation
- [electronjs.org/community#boilerplates](https://electronjs.org/community#boilerplates) - sample starter apps created by the community
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - a very basic starter Electron app
- [electron/simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for taking them further
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - an Electron app that teaches you how to use Electron
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo apps for the various Electron APIs

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
