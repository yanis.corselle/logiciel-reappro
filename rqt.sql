SELECT Paris00.sku,
       Paris00.name,
       Paris00.qty AS 'qtyParis00',
       Paris12.qty AS 'qtyParis12',
       Paris15.qty AS 'qtyParis15',
       ifnull(Paris00.qty, 0)+ifnull(Paris15.qty, 0)+ifnull(Paris12.qty, 0) AS 'TotalQty',
       ifnull(Paris12.sell5, 0)+ifnull(Paris00.sell5, 0)+ifnull(Paris00.sell5, 0) AS 'TotalVendu'
FROM Paris00
LEFT JOIN Paris12 ON Paris00.sku = Paris12.sku
LEFT JOIN Paris15 ON Paris00.sku = Paris15.sku
WHERE TotalVendu> 0
GROUP BY Paris00.sku
