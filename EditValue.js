const sqlite3 = require('sqlite3');


const path = require('path');
table = document.getElementById("table");
var TitlePage = document.getElementById('titre');


document.getElementById('P00').onclick = () => {
  TitlePage.textContent = "Paris00";
  verf();

  t = document.getElementById('table');

  tabl = document.getElementsByTagName("tbody")[0];
  t.removeChild(tabl);

  tabl = document.createElement("tbody");
  tabl.id = 'body';
  t.appendChild(tabl);


  var db = new sqlite3.Database('./db/file.db');


  db.serialize(function() {

    var rqt = rqt_all('Paris00');
    row = db.each(rqt, (err, row) => {
      Tav(row, 'P00');
    });
  });
  db.close();
}

document.getElementById('P15').onclick = () => {
  TitlePage.textContent = "Paris15";
  verf();

  t = document.getElementById('table');

  tabl = document.getElementsByTagName("tbody")[0];
  t.removeChild(tabl);

  tabl = document.createElement("tbody");
  tabl.id = 'body';
  t.appendChild(tabl);

  var db = new sqlite3.Database('./db/file.db');
  table = document.getElementById("table");
  db.serialize(function() {

    var rqt = rqt_all('Paris15');
    row = db.each(rqt, (err, row) => {
      Tav(row, 'P15');
      console.log(row)
    });
  });
  db.close();
}


document.getElementById('P12').onclick = () => {
  TitlePage.textContent = "Paris12";



  t = document.getElementById('table');
  verf();

  tabl = document.getElementsByTagName("tbody")[0];
  t.removeChild(tabl);

  tabl = document.createElement("tbody");
  tabl.id = 'body';
  t.appendChild(tabl);
  var db = new sqlite3.Database('./db/file.db');
  table = document.getElementById("table");

  db.serialize(function() {
    var rqt = rqt_all('Paris12');

    row = db.each(rqt, (err, row) => {
      Tav(row, "P12");
    });


  });
  db.close();
}



//TOTAL



document.getElementById('TOTAL').onclick = () => {
  TitlePage.textContent = "Toutes les boutiques";
  //suprssion de l'ancienne table
  t = document.getElementById('table');
  t.style.display = "none";

  tabl = document.getElementsByTagName("tbody")[0];
  t.removeChild(tabl);

  tabl = document.createElement("tbody");
  tabl.id = 'body';
  t.appendChild(tabl);
  var db = new sqlite3.Database('./db/file.db');
  table = document.getElementById("table");


  //suprresion et ciblage de Table Total

  tableTOTAL = document.getElementById('tableTOTAL');
  tableTOTAL.style.display = "table";

  bodyTotal = document.getElementById("bodyTotal");
  tableTOTAL.removeChild(bodyTotal);
  bodyTotal = document.createElement("tbody");
  bodyTotal.id = 'bodyTotal';
  tableTOTAL.appendChild(bodyTotal);
  var db = new sqlite3.Database('./db/file.db');
  tableTot = document.getElementById("tableTOTAL");


  //Lancement du remplissage

  db.serialize(function() {
    var rqt = "SELECT Paris00.sku, Paris00.name,"
    rqt += "Paris00.qty AS 'qtyParis00',"
    rqt += "Paris12.qty AS 'qtyParis12',"
    rqt += "Paris15.qty AS 'qtyParis15',"
    rqt += "ifnull(Paris12.qty, 0)+ifnull(Paris15.qty, 0)+ifnull(Paris00.qty, 0) AS 'TotalQty',"
    rqt += "ifnull(Paris12.sell5, 0)+ifnull(Paris15.sell5, 0)+ifnull(Paris00.sell5, 0) AS 'TotalVendu'"
    rqt += "FROM Paris00"
    rqt += "  LEFT JOIN Paris12 ON Paris00.sku = Paris12.sku"
    rqt += "  LEFT JOIN Paris15 ON Paris00.sku = Paris15.sku"
    rqt += "  WHERE TotalVendu> 0"
    rqt += "  GROUP BY Paris00.sku";
    console.log(rqt);
    row = db.each(rqt, (err, row) => {
      TavTotal(row);
    });


  });
  db.close();
}

const Tav = (row, boutique) => {

  var table = document.getElementById('body')
  var ligne = document.createElement("tr");

  // Création de l'élément Sku
  var skucell = document.createElement("td");
  var skutext = document.createTextNode(row.sku);
  skucell.appendChild(skutext);
  ligne.appendChild(skucell);

  // Création de l'élément Nom
  var nomcell = document.createElement("td");
  var nomtext = document.createTextNode(row.name);
  nomcell.appendChild(nomtext);
  ligne.appendChild(nomcell);

  // Création de l'élément Quantité

  var qtycell = document.createElement("td");
  var qtytext = document.createTextNode(row.qtyParis15);
  qtycell.appendChild(qtytext);
  ligne.appendChild(qtycell);

  // Création de l'élément qty12
  var qty12cell = document.createElement("td");
  var qty12text = document.createTextNode(row.qtyParis12);
  qty12cell.appendChild(qty12text);
  ligne.appendChild(qty12cell);

  // Création de l'élément qty00
  var qty00cell = document.createElement("td");
  var qty00text = document.createTextNode(row.qtyParis00);
  qty00cell.appendChild(qty00text);
  ligne.appendChild(qty00cell);

    // Création de l'élément besoin
  var besoincell = document.createElement("td");
  var besointext = document.createTextNode(row.Vendu);
  besoincell.appendChild(besointext);
  ligne.appendChild(besoincell);




  // Création de l'élément Recommandé

  var recommandecell = document.createElement("td");
  var qty12 = null
  if (row.qtyParis12 == null) {
    qty12 = 0

  } else {
    qty12 = parseInt(row.qtyParis12)
  }

  //console.log(row.sku +  ': (qty00) '+ qty00.toString() + '-'+'(Vendu) '+row.Vendu.toString()+ ' = '+ (qty00-parseInt(row.Vendu)).toString());

   if(boutique=="P12"){
     var qtySous = row.qtyParis12
  }else if (boutique=="P15") {
    var qtySous = row.qtyParis15

   }else{
     var qtySous = row.qtyParis00

   }


  if (parseInt(row.Vendu) - qtySous > -1) {
    var marque = "";
  } else {
    //  marque =row.Vendu
    marque = qtySous - parseInt(row.Vendu);
  }

  var recommandetext = document.createTextNode(marque);


  recommandecell.appendChild(recommandetext);
  ligne.appendChild(recommandecell);

  // Création de l'élément Valeur

  var valeurcell = document.createElement("td");
  var valeurtext = document.createElement("input");

  var a = document.createAttribute("ref");


  a.value = row.sku;
  valeurtext.setAttribute('type', 'number');

  valeurtext.setAttribute('name', 'take');
  valeurtext.setAttributeNode(a);


  if (parseInt(row.Vendu) - qtySous > 0) {
    var champs = parseInt(row.Vendu) - qtySous;
    valeurtext.value = champs;
    if (boutique == "P12" && (champs < row.qtyParis15) && (champs>row.qtyParis00)) {
      valeurcell.classList.add("NeedT");
    }
    else if (boutique == "P15" && (champs < row.qtyParis12) && (champs>row.qtyParis00)) {
      valeurcell.classList.add("NeedT");
    }

  } else {
    valeurtext.value = 0;


  }
  valeurcell.appendChild(valeurtext);
  ligne.appendChild(valeurcell);

  // Ajoute la ligne au tableau
  table.appendChild(ligne);
}


const rqt_all = (boutique) => {
  switch (boutique) {

    case "Paris00":
      var boutique2 = "Paris12";
      var boutique3 = "Paris15";
      break;
    case "Paris15":
      var boutique2 = "Paris12";
      var boutique3 = "Paris00";
      break;

    case "Paris12":
      var boutique2 = "Paris15";
      var boutique3 = "Paris00";
      break;
    default:
      console.log("erreur :" + boutique);
  }

  var rqt = "SELECT " + boutique + ".sku, " + boutique + ".name," + boutique + ".qty as 'qty" + boutique + "'," + boutique2 + ".qty as 'qty" + boutique2 + "',  " + boutique3 + ".qty as 'qty" + boutique3 + "' ,  " + boutique + ".sell5 as 'Vendu'," + boutique + ".besoin  FROM " + boutique + " LEFT JOIN " + boutique2 + " ON " + boutique + ".sku = " + boutique2 + ".sku LEFT JOIN  " + boutique3 + " ON " + boutique + ".sku =  " + boutique3 + ".sku WHERE " + boutique + ".sell5>'0' GROUP BY " + boutique + ".sku";
  console.log(rqt);
  return rqt;
}




document.getElementById('export').onclick = () => {
  var app = require('electron').remote;
  fs = require('fs');
  var dialog = app.dialog;

  var listeInput = document.getElementsByName('take');
  arr = [];
  arr.push(["sku", "barcode", "qty"]);
  for (var i = 0; i < listeInput.length - 1; i++) {
    var tempo = [];
    var e = new Object();
    e.ref = listeInput[i].getAttribute('ref');
    e.qty = listeInput[i].value;
    //console.log(e.qty);

    if (parseInt(e.qty) > 0) {

      tempo.push(e.ref, e.ref, e.qty);
      arr.push(tempo);
    }

  }

  var lineArray = [];
  arr.forEach(function(infoArray, index) {
    var line = infoArray.join(",");
    lineArray.push(index == 0 ? "" + line : line);
  });
  var csvContent = lineArray.join("\n");

  filename = dialog.showSaveDialog({}).then(result => {
    filename = result.filePath;
    if (filename === undefined) {
      alert('the user clicked the btn but didn\'t created a file');
      return;
    }
    fs.writeFile(filename + ".csv", csvContent, (err) => {
      if (err) {
        alert('an error ocurred with file creation ' + err.message);
        return
      }
      alert('Votre fichier à bien était créer');
    })
  }).catch(err => {
    alert(err)
  })
}




//Generate rows for all shops
TavTotal = (row) => {

  var table = document.getElementById('bodyTotal')
  var ligne = document.createElement("tr");

  // Création de l'élément Sku
  var skucell = document.createElement("td");
  var skutext = document.createTextNode(row.sku);
  skucell.appendChild(skutext);
  ligne.appendChild(skucell);

  // Création de l'élément Nom
  var nomcell = document.createElement("td");
  var nomtext = document.createTextNode(row.name);
  nomcell.appendChild(nomtext);
  ligne.appendChild(nomcell);

  // Création de l'élément Quantité

  var qtycell = document.createElement("td");
  var qtytext = document.createTextNode(row.qtyParis15);
  qtycell.appendChild(qtytext);
  ligne.appendChild(qtycell);

  // Création de l'élément qty12
  var qty12cell = document.createElement("td");
  var qty12text = document.createTextNode(row.qtyParis12);
  qty12cell.appendChild(qty12text);
  ligne.appendChild(qty12cell);

  // Création de l'élément qty00
  var qty00cell = document.createElement("td");
  var qty00text = document.createTextNode(row.qtyParis00);
  qty00cell.appendChild(qty00text);
  ligne.appendChild(qty00cell);

  // Création de l'élément TotalQty

  var qtyTotalcell = document.createElement("td");
  var qtyTotaltext = document.createTextNode(row.TotalQty);
  qtyTotalcell.appendChild(qtyTotaltext);
  ligne.appendChild(qtyTotalcell);

  // Création de l'élément besoin
  var besoincell = document.createElement("td");
  var besointext = document.createTextNode(row.TotalVendu);
  besoincell.appendChild(besointext);
  ligne.appendChild(besoincell);




  // Création de l'élément Recommandé

  var recommandecell = document.createElement("td");
  var TotalQty = null
  if (row.TotalQty == null) {
    TotalQty = 0

  } else {
    TotalQty = parseInt(row.TotalQty)
  }
  if (parseInt(row.TotalVendu) - TotalQty > -1) {
    var marque = "";

  } else {
    //  marque =row.Vendu
    marque = TotalQty - parseInt(row.TotalVendu);
  }

  var recommandetext = document.createTextNode(marque);


  recommandecell.appendChild(recommandetext);
  ligne.appendChild(recommandecell);

  // Création de l'élément Valeur

  var valeurcell = document.createElement("td");
  var valeurtext = document.createElement("input");


  var a = document.createAttribute("ref");

  a.value = row.sku;
  valeurtext.setAttribute('type', 'number');

  valeurtext.setAttribute('name', 'take');
  valeurtext.setAttributeNode(a);


  if (parseInt(row.TotalVendu) - TotalQty > -1) {
    valeurtext.value = parseInt(row.TotalVendu) - TotalQty;
  } else {
    valeurtext.value = 0;
  }
  valeurcell.appendChild(valeurtext);
  ligne.appendChild(valeurcell);

  // Ajoute la ligne au tableau
  table.appendChild(ligne);
}


const verf = () => {
  if (document.getElementById('table').style.display == "none") {
    tableTOTAL_Display = document.getElementById('tableTOTAL').style.display = "none";
    document.getElementById('table').style.display = "table"
  }

}
