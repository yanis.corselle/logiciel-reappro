const dialog = require('electron').remote.dialog;
var fs = require('fs');
const path = require('path');


const Database = require('sqlite-async')

document.getElementById('avatar').onchange = () => {

  //Get data file
  av = document.getElementById("avatar").files[0].path;
  document.getElementById('name').innerHTML=document.getElementById("avatar").files[0].name;

  var a = fs.readFileSync(av, 'utf8');
  var liste = a.split('\n');
  //Parse data csv to array
  var content = csvToArray(a);

  // Insert table
  generate_table(content);
}




async function generate_table(tab) {

  dbAsync = await Database.open('./db/file.db');

  // Clean Table
  await dbAsync.run("DROP  TABLE  if exists Paris00");
  await dbAsync.run("DROP  TABLE  if exists Paris15");
  await dbAsync.run("DROP  TABLE  if exists Paris12");
  await dbAsync.run("CREATE TABLE  if not exists Paris15 (sku,barcode, name,qty,sell5,besoin)");
  await dbAsync.run("CREATE TABLE  if not exists Paris12 (sku,barcode, name,qty,sell5,besoin)");
  await dbAsync.run("CREATE TABLE  if not exists Paris00 (sku,barcode, name,qty,sell5,besoin)");


  tableProduit = [];



  for (var i = 1; i < tab.length ; i++) {

    tableProduit.push(new Produit(tab[i]));

  }
    var progression =0;
    var i=0;
    data = fs.readFileSync('./ignorer.csv',
            {encoding:'utf8', flag:'r'});
    var x = data.split(';')
    tableProduit.forEach( async (produit,index)=>{
      produit.name = produit.name.replace("'", "")
           var boutique = ["Paris00",'Paris15','Paris12'];
           if(boutique.includes(produit.boutique)){
             verif = true
            for(var i= 0; i < x.length; i++)
            {
              
              if (x[i] == produit.sku)
              {
                verif = false
                console.log('false')
              }
            }
            if (verif == true){
              var rqt= `INSERT INTO ${produit.boutique} (sku,barcode, name,qty,sell5,besoin)VALUES('${produit.sku}','${produit.barcode}','${produit.name}','${produit.qty}','${produit.sell5}','${produit.besoin}')` ;
             await dbAsync.run(rqt);
             console.log(rqt);
            }
             

             //Update progress bar
             i +=1;
             var progression =  Math.trunc(((i/ tableProduit.length) * 100)).toString() + "%";
             document.getElementById("progress_bar_remplissage").style.width = progression;
             document.getElementById("indicateur_progress_bar").innerHTML = progression;

         }
});

var progression = "100%";
document.getElementById("progress_bar_remplissage").style.width = progression;
document.getElementById("indicateur_progress_bar").innerHTML = progression;
    await dbAsync.close();
    var btn = document.getElementById('suivant');
    btn.style.display = "block";

}





// Return array of string values, or NULL if CSV string not well formed.
 csvToArray = (text)=>  {
  let p = '',
    row = [''],
    ret = [row],
    i = 0,
    r = 0,
    s = !0,
    l;
  for (l of text) {
    if ('"' === l) {
      if (s && l === p) row[i] += l;
      s = !s;
    } else if (',' === l && s) l = row[++i] = '';
    else if ('\n' === l && s) {
      if ('\r' === p) row[i] = row[i].slice(0, -1);
      row = ret[++r] = [l = ''];
      i = 0;
    } else row[i] += l;
    p = l;
  }
  return ret;
};


 function Produit  (tab)  {
   if(tab[4]=="Paris0"){
     tab[4]+="0"
   };

  this.sku = tab[0];
  this.barcode = tab[0];
  this.name = tab[3];

  this.boutique=tab[4];
  this.qty = tab[5];
  //console.log( this.sku+' : sell5 :'+tab[6]+'/'+'2'+'='+ tab[6]/3+' environ '+ Math.ceil( tab[6]/2) );
  this.sell5 = Math.ceil( tab[10]/2);
  this.besoin = Math.ceil( tab[10]/2);

};
