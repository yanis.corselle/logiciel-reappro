import React, { useState,Component } from 'react';
import { SafeAreaView,Dimensions,StyleSheet,View,Image,ScrollView} from 'react-native';
import { Divider, Icon,Avatar,Input,Button, IndexPath,Layout, Select,Text, SelectItem  } from '@ui-kitten/components';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
//import ImagePickerExample from './ImagePickerExample';

     const useInputState = (initialValue = '') => {
  const [value, setValue] = React.useState(initialValue);
  return { value, onChangeText: setValue };
};


import { API, graphqlOperation } from "aws-amplify";
import * as queries from './src/graphql/queries';
import * as mutations from './src/graphql/mutations';
import * as subscriptions from './src/graphql/subscriptions';


import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";

const BackIcon = (style) => (
  <Icon {...style} name='arrow-back' />
);
const data = {
  labels: ["Glucide", "Proteine", "Lipides"], // optional
  data: [0.8, 0.5, 0.2]
};

const chartConfig={
  backgroundColor: "#ffffff",
  backgroundGradientFrom: "#ffffff",
  backgroundGradientTo: "#ffffff",
  decimalPlaces: 2, // optional, defaults to 2dp
  color: (opacity = 1) => `rgba(128, 128, 128, ${opacity})`,
  labelColor: (opacity = 1) => `rgba(86, 110, 235, ${opacity})`,
  style: {borderRadius: 2},
  propsForDots: {
    r: "6",
    strokeWidth: "2",
    stroke: "#ffa726"
  }
}


  const CreateUser = async (userInfos) => {

    var data = await API.graphql(graphqlOperation(mutations.createUser, {input: userInfos}));
    setAllergies(data.data);

    return data;

  }


export const ProfilScreen = (props)=>{

      let numericInput ={
        keyboardType:"numeric",
        name:"age"
      }

      const {
          oAuthUser: user,
          oAuthError: error,
          hostedUISignIn,
          facebookSignIn,
          googleSignIn,
          amazonSignIn,
          customProviderSignIn,
          signOut,
        } =props.route.params;

      let save = () => {
        //console.log(JSON.stringify({"nom":nom,"prenom" :prenom,"age": age,"poids":poids,"taille": taille}));
        let userInfos = {

          firstName: prenom.value,
          lastName: nom.value,
          gender: 'm',
          age: parseInt(age.value),
          taille: parseInt(taille.value),
          poids: parseInt(poids.value),
          UserToken: 'mlik',
          userProgrammeId: "null"
        };
        console.log(userInfos);
        //  CreateUser(userInfos);
}



               const nom = useInputState();
               const prenom = useInputState();
               const age = useInputState();
               const taille = useInputState();
               const poids = useInputState();

               age.keyboardType="numeric";
               taille.keyboardType="numeric";
               poids.keyboardType="numeric";

      return(
        <ScrollView>

          <View style={{alignItems:"center",backgroundColor:"white" ,paddingBottom:15}}>
          <Image style={styles.avatar}  source={{uri:user.attributes.picture }}/>
          <ImagePickerExample/>

              <Text category='h2'> Hello {user.attributes.name} </Text>
              <Text category='s1'> Vos informations de personnels</Text>

          </View>
          <Divider/>

          <View style={{width:"100%",alignItems:"center",backgroundColor:'#fafafa'}}>

              <View style={styles.container}>


                  <View>
                      <Text category='h4'>Mon nom </Text>
                      <Divider style={styles.divider}/>

                      <Input

                        placeholder={user.attributes.nom}
                  //    value={this.state.nom}
                  {...nom}

                      />
                  </View>


                <View>
                <Text category='h4'>Mon Prénom </Text>
                        <Divider style={styles.divider}/>

                        <Input


                                  placeholder='...'
                                //  value={prenom}
                                  //onChangeText={this.handleInputChange}
                                  {...prenom}

                                />
                          </View>


                          <View>
                              <Text category='h4'>Mon âge </Text>
                              <Divider style={styles.divider}/>

                              <Input
                                placeholder='...'
                              //  value={age}
                                {...numericInput}

                                {...age}
                              />


                        </View>

                        <View>
                            <Text category='h4'>Ma taille (en cm) </Text>
                            <Divider style={styles.divider}/>

                            <Input
                            name="taille"

                              placeholder='...'
                              //value={taille}
                              //onChangeText={this.handleInputChange}
                              {...taille}
                            />

                      </View>
                      <View>
                          <Text category='h4'>Mon poids (en kg) </Text>
                          <Divider style={styles.divider}/>


                          <Input
                          name="poids"

                            placeholder='...'
                            //value={poids}
                            //onChangeText={this.handleInputChange}
                            {...poids}

                          />

                    </View>


              <Button style={styles.button} status='primary' onPress={save}>  Sauvegarder </Button>
              </View>


            </View>
            </ScrollView>
      )

  }

/*
export const ProfilScreen = ({ navigation,route }) => {
  const [nom, setNom] = React.useState('');
  const [prenom, setPrenom] = React.useState('');
  const [age, setAge] = React.useState('');
  const [poids, setPoids] = React.useState('');
  const [taille, setTaille] = React.useState('');

  const {
    oAuthUser: user,
    oAuthError: error,
    hostedUISignIn,
    facebookSignIn,
    googleSignIn,
    amazonSignIn,
    customProviderSignIn,
    signOut,
  } =route.params;

  const navigateBack = () => {
  navigation.goBack();
  };

  const screenWidth = Dimensions.get("window").width;
 console.log(user.attributes);
 console.log(user);

 let save = () => {
   //console.log(JSON.stringify({"nom":nom,"prenom" :prenom,"age": age,"poids":poids,"taille": taille}));
   let userInfos = {

     firstName: prenom,
     lastName: nom,
     gender: 'm',
     age: parseInt(age),
     taille: parseInt(taille),
     poids: parseInt(poids),
     UserToken: 'mlik',
     userProgrammeId: "null"
   };
   console.log(userInfos);
     CreateUser(userInfos);

}
let numericInput ={
  keyboardType:"numeric"
}

return (

  <SafeAreaView style={{ flex: 1}}>

  </SafeAreaView>

  )
};
*/


const FormJH =(props) =>{

      return (
            <View>
                <Text category='h4'>{props.Label} </Text>
                <Divider style={styles.divider}/>

                <Input
                  placeholder={props.placeholder}
                  value={props.value}
                  onChangeText={nextValue => setValue(nextValue)}

                />
                {props.select && (

                      <Layout style={styles.container} level='1'>
                        <Select
                          multiSelect={true}
                          selectedIndex={selectedIndex}
                          onSelect={index => setSelectedIndex(index)}>
                          <SelectItem title='Option 1'/>
                          <SelectItem title='Option 2'/>
                          <SelectItem title='Option 3'/>
                        </Select>
                      </Layout>
                )}
          </View>
)
}
class ImagePickerExample extends React.Component {
 state = {
   image: null,
 };

 render() {
   let { image } = this.state;

   return (
     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
       <Button title="Changer votre image de profile" onPress={this._pickImage} />
       {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
     </View>
   );
 }

 componentDidMount() {
   this.getPermissionAsync();
 }

 getPermissionAsync = async () => {
   if (Constants.platform.ios) {
     const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
     if (status !== 'granted') {
       alert('Sorry, we need camera roll permissions to make this work!');
     }
   }
 };

 _pickImage = async () => {
   try {
     let result = await ImagePicker.launchImageLibraryAsync({
       mediaTypes: ImagePicker.MediaTypeOptions.All,
       allowsEditing: true,
       aspect: [4, 3],
       quality: 1,
     });
     if (!result.cancelled) {
       this.setState({ image: result.uri });
     }

     console.log(result);
   } catch (E) {
     console.log(E);
   }
 };
}


const styles = StyleSheet.create({
  avatar:{
    marginTop:10,
    justifyContent:"center",
    width: Dimensions.get('window').width/2,
    height: Dimensions.get('window').width/2,
    borderRadius: Dimensions.get('window').width,
          shadowColor: "#000",
        shadowOffset: {
        	width: 0,
        	height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

  },
  button:{
    marginTop:'2%',
    marginBottom:'5%',
  },
  container:{
    marginTop:'5%',
    width: "80%",
    justifyContent:'flex-start',
  },
  divider:{
    backgroundColor:"#dee3fa",
    width:'50%',
    marginBottom:"2%",
    marginTop:'1%',
    marginLeft:"1%"
}
});
